export const texts = [
  "WHEN I waked it was broad day, the weather clear, and the storm abated, so that the sea did not rage and swell as before. But that which surprised me most was, that the ship was lifted off in the night from the sand where she lay by the swelling of the tide, and was driven up almost as far as the rock which I at first mentioned, where I had been so bruised by the wave dashing me against it. This being within about a mile from the shore where I was, and the ship seeming to stand upright still, I wished myself on board, that at least I might save some necessary things for my use.",
  "A loud, echoing crack broke the sleepy silence like a gunshot; a cat streaked out from under a parked car and flew out of sight; a shriek, a bellowed oath and the sound of breaking china came from the Dursleys' living room, and as though this was the signal Harry had been waiting for he jumped to his feet, at the same time pulling from the waistband of his jeans a thin wooden wand as if he were unsheathing a sword--but before he could draw himself up to full height, the top of his head collided with the Dursleys' open window. The resultant crash made Aunt Petunia scream even louder.",
];

export const anecdote = [
  `Math, Physics, & Philosophy\n
  "Dean, to the physics department. "Why do I always have to give you guys so much money, for laboratories and expensive equipment and stuff. Why couldn't you be like the math department - all they need is money for pencils, paper and waste-paper baskets. Or even better, like the philosophy department. All they need are pencils and paper."`,
  `Tracker
  A family was visiting an Indian reservation when they happen upon an old tribesman laying face down in the middle of the road with his ear pressed firmly against the blacktop. The father of the family asked the old tribesman what he was doing.
  The tribesman began to speak... "woman, late thirties, three kids, one barking dog in late model, Four door station wagon, traveling at 65 m.p.h."
  "That's amazing" exclaimed the father. "You can tell all of that by just listening to the ground"?
  "No", said the old tribesman. "They just ran over me five minutes ago!"`,
];

export default { texts, anecdote };
