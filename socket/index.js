import * as config from "./config";
import partial from 'lodash.partial'
import { texts } from "../data";
import { commentType, Commentator } from "./commentator";

getRandomInt = partial(getRandomInt, 0);

export default io => {
  //user: userId
  const users = {};
  //room: [{user, userId, status, progress}, ...]
  const rooms = {};

  io.on("connection", socket => {
    const username = socket.handshake.query.username;
    let currentRoom = '';
    const commentator = new Commentator(username);

    if (users.hasOwnProperty(username)) {
        socket.emit('authorisation', false);
    }
    else {
        users[username] = '';
    }

    socket.emit('showRooms', rooms);

    socket.on('newRoom', (roomName) => {
        if (rooms.hasOwnProperty(roomName)) {
            socket.emit('roomCheck', false);
            return;
        }
        rooms[roomName] = [];
        io.emit('showRooms', rooms);
        showUsersInRoom(roomName);
    });

    socket.on('joinRoom', (roomName) => {
        socket.join(roomName);
        currentRoom = roomName;
        users[username] = roomName;
        rooms[roomName].push({
            username,
            userId: socket.id,
            status: false,
            progress: 0
        });
        io.emit('showRooms', rooms);
        showUsersInRoom(roomName);
        const comment  = commentator.sayComment(commentType.WELCOME).commentText;
        io.to(currentRoom).emit('commentator', comment);
    });

    socket.on('leaveRoom', () => leaveRoom());

    socket.on('updateProgressBar', (value) => {
        for (const user of rooms[currentRoom]) {
            if (user.username === username) {
                user.progress = value;
            }
        }
        showUsersInRoom(currentRoom);
        if (value === 100) {
            const comment = new Commentator(username).sayComment(commentType.CONGRATS).commentText;
            io.to(currentRoom).emit('commentator', comment);
        }
    });

    setInterval(() => {
        const comment = new Commentator(username).sayComment(commentType.ANECDOTE).commentText;
        io.to(currentRoom).emit('commentator', comment)
    }, 7000);

    socket.on('disconnect', () => {
        if(currentRoom !== '') {
            leaveRoom();
        }
        delete users[username];
    });

    socket.on('readyCheck', (boolCheck) => {
        let counter = 0;
        for (const user of rooms[currentRoom]) {
            if (user.username === username) {
                user.status = boolCheck;
                socket.to(currentRoom).emit('showGamers', rooms[currentRoom]);
                if (boolCheck) {
                    const comment  = commentator.sayComment(commentType.READY).commentText;
                    io.to(currentRoom).emit('commentator', comment);
                }
            }
            if (user.status) {
                counter++;
            }
        }
        if (counter === rooms[currentRoom].length) {
            const textNumber = getRandomInt(texts.length);
            io.emit('startGame', texts[textNumber]);
            const comment = new Commentator(username).sayComment(commentType.START).commentText;
            io.to(currentRoom).emit('commentator', comment);
        }
    })

    function leaveRoom() {
        socket.leave(currentRoom);
        const room = currentRoom;
        users[username] = '';
        for (const user of rooms[currentRoom]) {
            if (user.username === username) {
                rooms[currentRoom].splice(rooms[currentRoom].indexOf(user), 1);
            }
        }
        if (Object.keys(rooms[currentRoom]).length === 0) {
            delete rooms[currentRoom];
        }
        currentRoom = '';
        io.emit('showRooms', rooms);
        showUsersInRoom(room);
    }

    function showUsersInRoom(roomName) {
        io.to(roomName).emit('showGamers', rooms[roomName]);
    }
  });
};

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}