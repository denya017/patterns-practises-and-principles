import curry from 'lodash.curry';
import { anecdote } from '../data'
const commentType = Object.freeze({
    WELCOME:   Symbol("welcome"),
    READY:      Symbol("ready"),
    START:     Symbol("start"),
    CONGRATS:  Symbol("congrats"),
    ANECDOTE:  Symbol("anecdote"),
});


getRandomInt = curry(getRandomInt);

class Commentator {
    constructor(username) {
        this.username = username;
    }

    sayComment(key) {
        let comment;
        if (key === commentType.WELCOME) {
            comment = new Welcome(this.username);
        } else if (key === commentType.READY) {
            comment = new Ready(this.username);
        } else if (key === commentType.START) {
            comment = new Start(this.username);
        } else if (key === commentType.CONGRATS) {
            comment = new Congrats(this.username);
        } else if (key === commentType.ANECDOTE) {
            comment = new Anecdote();
        }
        return comment;
    }

}

class Welcome {
    constructor(username) {
        this.commentText = `Hello, ${username}! Welcome to the race!`;
    }
}

class Ready {
    constructor(username) {
        this.commentText = `${username} is ready, what about the others?`;
    }
}

class Start {
    constructor() {
        this.commentText = `Everyone is ready, so we can start the race. Go!`;
    }
}

class Congrats {
    constructor(username) {
        this.commentText = `Congratulations to ${username} on the victory in the races!`;
    }
}

class Anecdote {
    constructor() {
        this.commentText = anecdote[getRandomInt(0)(anecdote.length)];
    }
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}


export {commentType, Commentator}